**Problem and solution:**
To load the list of users with CRUD features, we use 
Retrofit to access data, save them in repository and present 
them to activity/fragment


**Left off:**
* add new  user when click on FAB button
* detail of user when moving to UserInfoActivity by reusing the card layout
* add delete icon in UserInfoActivity and delete user with alert dialog 
* click on website in card view should open the website in a browser



**Improvments:**
* Save data in SQLITE database for data persistance
* Add dependency injection
* finish all view model tests
* add repository tests

 