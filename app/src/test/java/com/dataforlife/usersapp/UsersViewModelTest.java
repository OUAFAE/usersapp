package com.dataforlife.usersapp;


import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.dataforlife.usersapp.pojo.User;
import com.dataforlife.usersapp.repository.UserRepository;
import com.dataforlife.usersapp.viewmodel.UsersViewModel;
import com.dataforlife.usersapp.views.UsersAdapter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UsersViewModelTest {


    private UsersViewModel usersViewModel;
    private MutableLiveData<List<User>> usersObservable = new MutableLiveData<>();
    private List<User> userList;



    @Mock
    private UserRepository userRepository;
    @Mock
    private UsersAdapter usersAdapter;
    @Mock
    private Application applicationMock;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void init() {
        applicationMock = Mockito.mock(Application.class);
        usersViewModel = new UsersViewModel(userRepository);
        //usersAdapter = new UsersAdapter(applicationMock.getApplicationContext(),Arrays.asList(message),messageViewModel);
        userList = new ArrayList<User>(
                Arrays.asList(new User(0,"user one"),new User(1,"user one"), new User(2,"user one")));
        when(usersViewModel.getUsers().thenReturn(userList));
    }

    /**
     * Test in case of success =>
     */
    @Test
    public void getUsersTest() {
        usersViewModel.getUsers();
    }










}