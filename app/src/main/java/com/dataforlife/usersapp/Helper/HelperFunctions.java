package com.dataforlife.usersapp.Helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

public class HelperFunctions {


    /**
     * Open the phone dialer if possible
     *
     * @param context
     * @param phoneNumber
     */
    public static void openDialer(Context context, String phoneNumber ) {
        Uri call = Uri.fromParts("tel", phoneNumber, null);
        Intent intent = new Intent(Intent.ACTION_DIAL, call);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) { //when device has no phone app, callback if available

        }
    }

    /**
     * Send email by pre-filling
     *
     * @param context
     * @param email
     * @param description
     */
    public static void sendEmail(Context context, @NonNull String email, String description  ) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        intent.putExtra(Intent.EXTRA_TEXT, description);
        context.startActivity(intent);


    }

}
