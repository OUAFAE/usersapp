package com.dataforlife.usersapp.views;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dataforlife.usersapp.Helper.HelperFunctions;
import com.dataforlife.usersapp.R;
import com.dataforlife.usersapp.pojo.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder>  {

    private List<User> users;
    private ListItemClickListener  listItemClickListener;


    /**
     * Default constructor
     *
     * @param list
     */
    public UsersAdapter(ListItemClickListener listener, @NonNull List<User> list) {
        users = list;
        listItemClickListener = listener;
    }

    @Override
    public UsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new UsersViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(final UsersViewHolder holder, int position) {
        if (users != null) {
            final User thisUser = users.get(holder.getAdapterPosition());
            if (holder.title != null) {
                holder.title.setText(thisUser.getName());
            }

            if (holder.username != null && thisUser.getUsername() != null) {
                holder.username.setText(thisUser.getUsername());
            }
            if (holder.website != null && thisUser.getWebsite() != null) {
                holder.website.setText(thisUser.getWebsite());
                holder.website.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listItemClickListener.onItemSelect(holder.getAdapterPosition(), thisUser);
                }
            });

            if (holder.buttonContact != null) {
                holder.buttonContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(thisUser.getEmail())) {
                            HelperFunctions.sendEmail(holder.buttonContact.getContext(), thisUser.getEmail(), "sent via DataForLife");
                        }
                    }
                });
            }

            if (!TextUtils.isEmpty(thisUser.getPhone())) {
                if (holder.buttonCall != null) {
                    holder.buttonCall.setVisibility(View.VISIBLE);
                }

                holder.buttonCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HelperFunctions.openDialer(holder.buttonCall.getContext(), thisUser.getPhone());
                    }
                });
            } else {
                if (holder.buttonCall != null) {
                    holder.buttonCall.setVisibility(View.GONE);
                }
            }
        }
    }



    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }

    /**
     * see R.fragment_dialog_first_install.item_article_photo.xml
     */
    class UsersViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        TextView title, username, website;
        CardView cardView;
        FrameLayout buttonCall, buttonContact;
        View divider;

        UsersViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            username = v.findViewById(R.id.username);
            website = v.findViewById(R.id.website);
            cardView = v.findViewById(R.id.card_view);
            buttonCall = v.findViewById(R.id.call);
            buttonContact = v.findViewById(R.id.message);

        }
    }
}
