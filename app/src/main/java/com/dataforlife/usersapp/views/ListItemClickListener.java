package com.dataforlife.usersapp.views;


import com.dataforlife.usersapp.pojo.User;

public interface ListItemClickListener {
    /**
     * Callback to observer of which item has been clicked
     *
     * @param position of the item (if that is useful)
     * @param item     itself
     */
    void onItemSelect(int position, User item);


}
