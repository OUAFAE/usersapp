package com.dataforlife.usersapp.views;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.dataforlife.usersapp.Helper.VerticalDividerItemDecoration;
import com.dataforlife.usersapp.R;
import com.dataforlife.usersapp.pojo.User;
import com.dataforlife.usersapp.repository.UserRepository;
import com.dataforlife.usersapp.viewmodel.UsersViewModel;

import java.util.List;

public class UsersFragment extends Fragment implements ListItemClickListener{

    private UsersAdapter adapter;
    private RecyclerView recyclerView;
    private UsersViewModel usersViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getContentViewLayout(), container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        usersViewModel = new UsersViewModel(new UserRepository());
        if (getItemDecoration() != null) {
            recyclerView.addItemDecoration(getItemDecoration());
        }
        loadUsers();
        observeSearchSuggestions();
    }


    protected int getContentViewLayout() {
        return R.layout.fragment_main;
    }


    /**
     * This defines the divider between each items. Override this to use your custom decoration
     *
     * @return
     */
    protected RecyclerView.ItemDecoration getItemDecoration() {
        return new VerticalDividerItemDecoration(getContext(), R.dimen.standard_left_right_margin);
    }

    /**
     * Function to load users and save result
     */
    public void loadUsers() {
          usersViewModel.loadUsers();
    }


    /**
     * Expose the LiveData users query so the UI can observe it.
     */
    public void observeSearchSuggestions( ) {
        usersViewModel.getUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                if (recyclerView != null && users != null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    adapter = new UsersAdapter (UsersFragment.this, users);
                    recyclerView.setAdapter(adapter);
                }
            }
        });
    }

    /**
     * @param position of the user
     * @param user  is needed to call startActivity
     */
    @Override
    public void onItemSelect(int position, User user) {
        if (user != null && getActivity() != null) {
            Intent intent = UserInfoActivity.intentBuilder(getActivity(), user);
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


}