package com.dataforlife.usersapp.network;

import android.support.annotation.NonNull;

import com.dataforlife.usersapp.pojo.User;
import com.dataforlife.usersapp.pojo.UserBody;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ApiInterface {


    @GET("users")
    Call<List<User>> getUsers();

    @GET("user/{id}")
    Call<User> getUserDetails(
            @Path("id") int id
    );



    @POST("users")
    Call<User> addUser(@Body UserBody params);

    @DELETE("user/{id}")
    Call<Void> deleteUser(
            @Path("id") int ideabookId
    );
}