package com.dataforlife.usersapp.network;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.dataforlife.usersapp.pojo.User;
import com.dataforlife.usersapp.pojo.UserBody;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Callback;


public class ApiMediator {
    private static final String TAG = "ApiMediator"; //NON-NLS
    private final ApiInterface apiService;
    private Context context;

    public ApiMediator(Fragment fragment) {
        apiService = new ApiClient().getService(ApiInterface.class);
    }

    public ApiMediator() {
        apiService = new ApiClient().getService(ApiInterface.class);
    }

    /**
     * Get users list.
     *
     * @param callback the callback
     */
    public void getUsers(Callback<List<User>> callback) {

        apiService.getUsers().enqueue(callback);
    }

    /**
     * Getuser object
     *
     * @param id   id of the user
     * @param callback the callback
     */
    public void getUserDetail(int id,  Callback<User> callback) {
        Type responseType = new TypeToken<User>() {
        }.getType();
        apiService.getUserDetails(id).enqueue(callback);
    }

    /**
     * Create User
     *
     * @param params   is the info to create new user
     *
     **/
    public void addUser(UserBody params, Callback<User> callback) {
         apiService.addUser(params).enqueue(callback);

     }


    /**
     * Create User
     * @param id   id of the user
     * @param callback the callback
     */
    public void deleteUser(int id, Callback<Void> callback) {
        //Need responseType to parse cache response.
        Type responseType = new TypeToken<Void>() {
        }.getType();
        apiService.deleteUser(id).enqueue(callback);

    }


}