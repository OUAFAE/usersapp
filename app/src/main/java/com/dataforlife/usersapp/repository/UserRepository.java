package com.dataforlife.usersapp.repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.dataforlife.usersapp.network.ApiMediator;
import com.dataforlife.usersapp.pojo.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {

    private ApiMediator apiMediator;
    private MutableLiveData<User> userSource = new MutableLiveData<>();
    private MutableLiveData<List<User>> usersSource = new MutableLiveData<>();
    private MutableLiveData<Exception> error = new MutableLiveData<>();


    public UserRepository( ) {
        this.apiMediator = new ApiMediator();
    }

    public void loadUsers() {
        apiMediator.getUsers( new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                usersSource.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                if (t instanceof Exception) {
                    Exception exception = (Exception) t;
                    error.setValue((Exception) t);
                }
            }
        });
    }

    public void loadUserDetails(int id) {
        apiMediator.getUserDetail(id, new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                userSource.postValue(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (t instanceof Exception) {
                    Exception exception = (Exception) t;
                    error.setValue((Exception) t);
                }
            }
        });
    }

    public MutableLiveData<User> getUserSource() {
        return userSource;
    }


    public MutableLiveData<List<User>> getUsersSource() {
        return usersSource;
    }

    public MutableLiveData<Exception> getError() {
        return error;
    }
}
