package com.dataforlife.usersapp.pojo;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Geo {

    @Nullable
    @SerializedName("lat")
    private long lat;
    @Nullable
    @SerializedName("lng")
    private long lng;

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }
}
