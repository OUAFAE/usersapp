package com.dataforlife.usersapp.pojo;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Company {

    @Nullable
    @SerializedName("name")
    private String name;
    @Nullable
    @SerializedName("catchPhrase")
    private String catchPhrase;
    @Nullable
    @SerializedName("bs")
    private String bs;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(@Nullable String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    @Nullable
    public String getBs() {
        return bs;
    }

    public void setBs(@Nullable String bs) {
        this.bs = bs;
    }
}
