package com.dataforlife.usersapp.pojo;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Address {

    @Nullable
    @SerializedName("street")
    private String street;
    @Nullable
    @SerializedName("suite")
    private String suite;
    @Nullable
    @SerializedName("city")
    private String city;
    @Nullable
    @SerializedName("zipcode")
    private String zipcode;
    @Nullable
    @SerializedName("geo")
    private Geo geo;

    @Nullable
    public String getStreet() {
        return street;
    }

    public void setStreet(@Nullable String street) {
        this.street = street;
    }

    @Nullable
    public String getSuite() {
        return suite;
    }

    public void setSuite(@Nullable String suite) {
        this.suite = suite;
    }

    @Nullable
    public String getCity() {
        return city;
    }

    public void setCity(@Nullable String city) {
        this.city = city;
    }

    @Nullable
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(@Nullable String zipcode) {
        this.zipcode = zipcode;
    }

    @Nullable
    public Geo getGeo() {
        return geo;
    }

    public void setGeo(@Nullable Geo geo) {
        this.geo = geo;
    }
}
