package com.dataforlife.usersapp.pojo;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class User {

    @Nullable
    @SerializedName("id")
    private int id;
    @Nullable
    @SerializedName("name")
    private String name;
    @Nullable
    @SerializedName("username")
    private String username;
    @Nullable
    @SerializedName("email")
    private String email;
    @Nullable
    @SerializedName("adddress")
    private Address address;
    @Nullable
    @SerializedName("phone")
    private String phone;
    @Nullable
    @SerializedName("website")
    private String website;
    @Nullable
    @SerializedName("company")
    private Company company;

    public User(int id, @Nullable String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getUsername() {
        return username;
    }

    public void setUsername(@Nullable String username) {
        this.username = username;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    public Address getAddress() {
        return address;
    }

    public void setAddress(@Nullable Address address) {
        this.address = address;
    }

    @Nullable
    public String getPhone() {
        return phone;
    }

    public void setPhone(@Nullable String phone) {
        this.phone = phone;
    }

    @Nullable
    public String getWebsite() {
        return website;
    }

    public void setWebsite(@Nullable String website) {
        this.website = website;
    }

    @Nullable
    public Company getCompany() {
        return company;
    }

    public void setCompany(@Nullable Company company) {
        this.company = company;
    }
}
