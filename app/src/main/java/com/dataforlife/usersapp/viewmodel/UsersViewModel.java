package com.dataforlife.usersapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;

import com.dataforlife.usersapp.pojo.User;
import com.dataforlife.usersapp.repository.UserRepository;

import java.util.List;

public class UsersViewModel {
    private UserRepository repository;

    public UsersViewModel(UserRepository userRepository) {
        this.repository = userRepository;
    }

    public MutableLiveData<List<User>> getUsers() {

        return repository.getUsersSource();
    }

    public void loadUsers() {
        repository.loadUsers();
    }

}
