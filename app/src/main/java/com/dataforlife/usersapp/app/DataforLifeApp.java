package com.dataforlife.usersapp.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDexApplication;

import com.dataforlife.usersapp.BuildConfig;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import timber.log.Timber;

public class DataforLifeApp  extends MultiDexApplication {

    //A single Okhttp object for retrofit
    private static OkHttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        // Normal app init code...
        //if (BuildConfig.DEBUG) {
        Timber.plant(new Timber.DebugTree());
        Stetho.initializeWithDefaults(this);

    }

    public static OkHttpClient getOkhttpClient() {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
        }
        return client;
    }
}